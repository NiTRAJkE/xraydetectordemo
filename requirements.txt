onnxruntime==1.18.0
numpy==1.24.3
opencv-python==4.10.0.82
uvicorn==0.23.2
fastapi==0.102.0
torch==2.3.1
torchvision==0.18.1
