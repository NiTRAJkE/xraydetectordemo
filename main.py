from fastapi import FastAPI, File, UploadFile
from fastapi.responses import HTMLResponse, FileResponse, Response
import onnxruntime
import numpy as np
import cv2
from copy import deepcopy

import utils

COLORS = {'Atelectasis': (255, 0, 0),
          'Nodule/Mass': (150, 250, 70),
          'Effusion': (0, 0, 200),
          'Cardiomegaly': (40, 240, 120),
          'Infiltrate': (0, 200, 20),
          'Pneumonia': (120, 120, 180),
          'Pneumothorax': (255, 255, 35)}

CLASSES = {0: 'Atelectasis',
           1: 'Cardiomegaly',
           2: 'Effusion',
           3: 'Infiltrate',
           4: 'Nodule/Mass',
           5: 'Pneumonia',
           6: 'Pneumothorax'}

app = FastAPI()


@app.get("/", response_class=HTMLResponse)
async def main():
    with open("index.html") as f:
        return HTMLResponse(content=f.read(), status_code=200)


@app.post("/predict/")
async def predict(file: UploadFile = File(...)):
    image = np.frombuffer(await file.read(), np.uint8)
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    image_s = deepcopy(image)

    im = utils.letterbox(image, 320)[0]  # padded resize

    im = im.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
    im = im.astype(np.float32) / 255.0

    if len(im.shape) == 3:
        im = im[None]  # expand for batch dim

    providers = ['CPUExecutionProvider']
    multi_class_session = onnxruntime.InferenceSession('models/multi_class.onnx', providers=providers)
    output_names = [x.name for x in multi_class_session.get_outputs()]
    y = multi_class_session.run(output_names, {multi_class_session.get_inputs()[0].name: im})
    det = np.array(utils.non_max_suppression(y, [0.5, 0.6, 0.8, 0.55, 0.65, 0.45, 0.5], 0.3, multi_label=True)[0])

    if len(det):
        det[:, :4] = utils.scale_boxes(im.shape[2:], det[:, :4], image.shape).round()

    for box in det:
        x1, y1, x2, y2 = map(int, box[:4])
        cv2.rectangle(image, (x1, y1), (x2, y2), COLORS[CLASSES[box[-1]]], 2)
        cv2.putText(image, CLASSES[box[-1]], (x1, y1 - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)

    providers = ['CPUExecutionProvider']
    single_class_session = onnxruntime.InferenceSession('models/multi_class.onnx', providers=providers)
    output_names = [x.name for x in single_class_session.get_outputs()]
    y = single_class_session.run(output_names, {single_class_session.get_inputs()[0].name: im})
    det = np.array(utils.non_max_suppression(y, 0.7, 0.3, multi_label=False)[0])

    if len(det):
        det[:, :4] = utils.scale_boxes(im.shape[2:], det[:, :4], image_s.shape).round()

    for box in det:
        x1, y1, x2, y2 = map(int, box[:4])
        cv2.rectangle(image_s, (x1, y1), (x2, y2), COLORS[CLASSES[box[-1]]], 2)

    cv2.putText(image, 'Multi-class detector', (50, 50),
                cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 150, 80), 3, cv2.LINE_AA)
    cv2.putText(image_s, 'Single-class detector', (50, 50),
                cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 150, 80), 3, cv2.LINE_AA)

    image = np.concatenate([image, image_s])

    image_bytes = cv2.imencode('.jpg', image)[1].tostring()
    
    return Response(content=image_bytes, media_type="image/jpg")


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)
