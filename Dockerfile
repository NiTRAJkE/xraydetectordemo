FROM python:3.10-slim

WORKDIR /app

COPY ./requirements.txt /app/requirements.txt

RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  -y

RUN pip install --no-cache-dir -r /app/requirements.txt
RUN pip install python-multipart

COPY . /app

CMD ["python", "main.py"]
